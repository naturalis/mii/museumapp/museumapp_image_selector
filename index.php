<?php

    // include_once("auth.php");
    include_once("class.imageSelector.php");

    $dbFolder = $_ENV["IMAGE_SELECTOR_PATH"];
    $dbBakFolder = $_ENV["IMAGE_SELECTOR_BAK_PATH"];

    $m = new imageSelector;

    $m->setDatabaseFolder($dbFolder);
    $m->setBackupFolder($dbBakFolder);
    $m->initialize();

    if (isset($_POST) && isset($_POST["action"]) && $_POST["action"]=="reset" && isset($_POST["unitId"]))
    {
        $m->resetSavedUnitid($_POST["unitId"]);
    }
    else
    if (isset($_POST) && isset($_POST["images"]) && isset($_POST["unitId"]))
    {
        foreach ($_POST["images"] as $key => $value)
        {
            $data = json_decode($value);
            if ($data->exclude==true)
            {
                $m->setExcludedUrl($data->url);
            }
            else
            {
                $m->setIncludedUrl($data->url);
            }
        }
        $m->saveUrls($_POST["unitId"]);
        $prev = $m->getSavedUrls($_POST["unitId"]);

    }
    else
    if (isset($_POST) && isset($_POST["action"]) && $_POST["action"]=="skip")
    {
        $m->saveSkip($_POST["unitId"]);
        $prev = $m->getSkippedUrls($_POST["unitId"]);
    }

    $batch = $m->getNextUnitId();
    $remain = $m->getRemainingUnitIdCount();

?>
<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet" type="text/css" href="css/style.css" media="screen">
<link rel="stylesheet" type="text/css" media="screen" title="default" href="css/inline_templates.css" />

<script type="text/javascript" src="js/inline_templates.js"></script>
<script type="text/javascript" src="js/main.js"></script>

<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
</head>


<body>
<div class="panel thumbs">
<?php

    if (empty($batch))
    {
        echo '<span class="title">Geen objecten over</span>';
    }
    else
    {
        echo sprintf(
            '<span class="title">%s (<span class="count-selected">%s</span>/%s) [%s]</span>',
            $batch[0]["unitid"],count($batch),count($batch),$remain);        
        echo '<input type="button" name="save" value="save" onclick="save();">';
        echo '<input type="button" name="skip" value="skip" onclick="skip();">';
    }

?>
    <div class="thumbs-content">
    </div>
</div>


<table class="structure">
    <tr>
        <td>
            <div class="panel images">
            </div>
        </td>
        <td>
            <div class="panel control">
            <?php
                if(!empty($prev['unitid']))
                {
            ?>    

                <table>
                    <tr>
                        <td colspan="2">previous: <b><?php echo $prev['unitid']; ?></b></td>
                    </tr>
                    <tr>
                        <td style="vertical-align:top">
                                <table>
                                <tr>
                                    <td>included:</td>
                                </tr>

            <?php
                    foreach ($prev['urls'] as $val)
                    {
                        echo sprintf('<tr><td><img class="thumb" src="%s"></td></tr>',$val);
                    }
            ?>
                            </table>
                        </td>
                        <td style="vertical-align:top">
                            <table>
                                <tr>
                                    <td>excluded:</td>
                                </tr>
            <?php
                    foreach ($prev['excluded_urls'] as $val)
                    {
                        echo sprintf('<tr><td><img class="thumb" src="%s"></td></tr>',$val);
                    }
            ?>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <form method="post" >
                                <input type="hidden" name="action" value="reset">
                                <input type="hidden" name="unitId" value="<?php echo $prev['unitid']; ?>">
                                <input type="submit" value="reset" />
                            </form>
                        </td>
                    </tr>
                </table>
            <?php
                }
            ?>    

            </div>
        </td>
    </tr>
</table>
<div class="footer">
    Ga naar:
    <ul>
        <li><a href="overview.php?type=ready">overzicht verwerkte objecten</a></li>
        <li><a href="overview.php?type=skipped">overzicht overgeslagen objecten</a></li>
    </ul>
</div>

<script type="text/javascript">
$(document).ready(function()
{
    $.fn.classList = function() {return this[0].className.split(/\s+/);};

    acquireInlineTemplates();

<?php
    echo "unitId = '".$batch[0]["unitid"]."';\n";
    foreach ($batch as $key => $val)
    {
        echo "addImage('".$val['url']."');\n";
    }
?>
    printThumbnails();
    printImages();
    attachEvents();
});

</script>

<div class="inline-templates" id="imageTableTpl">
<!--
    <table>
        %body%
    </table>
-->
</div>

<div class="inline-templates" id="imageImageTpl">
<!--
    <tr data-key="%key%">
        <td class="medialib">%key%</td>
        <td class="medialib divider">
            <img onclick="popupimage(this);" src="%src%" data-key="%key%" class="medialib" title="%src%"/>
            <div class="exclude" data-key="%key%">%exclude%</div>
        </td>
        
        <td class="divider">
            <span data-key="%key%" class="checkbox" onclick="toTheTop(this);">&#128285;</span>
        </td>
        <td class="divider">%up%</td>
        <td class="divider">%down%</td>
        <td class="divider"><img class="icon delete" data-key="%key%" src="img/x-png-icon-25.jpg"></td>
    </tr>
-->
</div>

<div class="inline-templates" id="imageIconUpTpl">
<!--
    <img class="icon up" data-key="%key%" src="img/svg-black-up-arrow-icon-1.png">
-->
</div>

<div class="inline-templates" id="imageIconDownTpl">
<!--
    <img class="icon down" data-key="%key%" src="img/svg-black-down-arrow-icon-1.png">
-->
</div>

<div class="inline-templates" id="thumbsTableTpl">
<!--
    <table><tr>
        %body%
    </tr></table>
-->
</div>

<div class="inline-templates" id="thumbsTdTpl">
<!--
    <td data-key="%key%" class="mediathumb">
        <img src="%src%" data-key="%key%" class="thumb"/>
        <div class="thumb exclude" data-key="%key%" >%exclude%</div>
    </td>
-->
</div>

<div class="inline-templates" id="thumbsRowEndTpl">
<!--
    </tr><tr>
-->
</div>

<div class="inline-templates" id="overlayCrossTpl">
<!--
&#10799;
-->
</div>

</body>
