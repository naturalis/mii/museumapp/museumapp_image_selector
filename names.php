<?php

    // include_once("auth.php");
    include_once("class.imageSelector.php");

    $m = new imageSelector;

    if (isset($_POST) && isset($_POST["images"]) && isset($_POST["unitId"]))
    {
        foreach ($_POST["images"] as $key => $value)
        {
            $data = json_decode($value);
            if ($data->exclude==true)
            {
                $m->setExcludedUrl($data->url);
            }
            else
            {
                $m->setIncludedUrl($data->url);
            }
        }
        $m->saveUrls($_POST["unitId"]);
    }

    $batch = $m->getNextScientificName();
    

?>
<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet" type="text/css" href="css/style.css" media="screen">
<link rel="stylesheet" type="text/css" media="screen" title="default" href="css/inline_templates.css" />
<script type="text/javascript" src="js/inline_templates.js"></script>
<script
  src="https://code.jquery.com/jquery-3.4.1.min.js"
  integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
  crossorigin="anonymous"></script>
<script type="text/javascript">

var unitId="";
var images=Array();
var thumbsPerLine = 8;

function addImage( img, unitid )
{
    images.push( { url:img, unitid:unitid, key: images.length, exclude: false } );
}

function printThumbnails()
{
    buffer = Array();
    for(var i=0;i<images.length;i++)
    {
        buffer.push(fetchTemplate( 'thumbsTdTpl' )
            .replace('%src%',images[i].url.replace('large', 'medium'))
            .replace(/%key%/g,images[i].key)
            .replace('%exclude%',fetchTemplate( 'overlayCrossTpl' ))
            .replace('%unitid%',images[i].unitid)
        );

        if (i>0 && ((i+1) % thumbsPerLine)==0)
        {
            buffer.push(fetchTemplate( 'thumbsRowEndTpl' ));
        }
    }

    $('div.thumbs-content').html(fetchTemplate( 'thumbsTableTpl' ).replace('%body%',buffer.join("\n")));
}

function printImages()
{
    buffer = Array();
    for(var i=0;i<images.length;i++)
    {
        buffer.push(fetchTemplate( 'imageImageTpl' )
//            .replace('%up%',i>0 ? fetchTemplate( 'imageIconUpTpl' ):'')
//            .replace('%down%',i==(images.length-1) ? '' : fetchTemplate( 'imageIconDownTpl' ))
            .replace('%up%',fetchTemplate( 'imageIconUpTpl' ))
            .replace('%down%',fetchTemplate( 'imageIconDownTpl' ))
            .replace(/%src%/g,images[i].url)
            .replace(/%key%/g,images[i].key)
            .replace('%exclude%',fetchTemplate( 'overlayCrossTpl' ))
            .replace('%unitid%',images[i].unitid)
            
        );
    }

    $('div.images').html(fetchTemplate( 'imageTableTpl' ).replace('%body%',buffer.join("\n")));
}

function attachEvents()
{
    $('img.icon.delete,img.thumb,.exclude,img.medialib').on('click',function()
    {
        var key = $(this).attr('data-key');
        for(var i=0;i<images.length;i++)
        {
            if (images[i].key==key)
            {
                images[i].exclude = !images[i].exclude;
                $('.exclude[data-key='+key+']').toggle(images[i].exclude);
            }
        }

        var excluded=0;
        for(var i=0;i<images.length;i++)
        {
            excluded += images[i].exclude ? 1 : 0;
        }
        $('.count-selected').html(images.length-excluded);
    });

    $('img.icon.up,img.icon.down').on('click',function()
    {
        var downwards = $.inArray( "down", $(this).classList())==1;
        var key = $(this).attr('data-key');
        for(var i=0;i<images.length;i++)
        {
            if (images[i].key==key)
            {
                var row = $('tr[data-key='+key+']').closest('tr');
                var td = $('td.mediathumb[data-key='+key+']').closest('td');

                if (downwards)
                {
                    row.insertAfter( row.next() );
                    td.insertAfter( td.next() );                    
                }
                else
                {
                    row.prev().insertAfter(row);
                    td.prev().insertAfter(td);
                }
            }
        }
    });

}

function save()
{
    var form = $('<form action="" method="POST"></form>');
    form.appendTo('body')
    var n=0;
    $('img.medialib').each(function()
    {
        var key = $(this).attr('data-key');
        for(var i=0;i<images.length;i++)
        {
            if (images[i].key==key)
            {
                var exclude = images[i].exclude;
                var url = images[i].url;
            }
        }

        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", "images["+(n++)+"]");
        hiddenField.setAttribute("value", JSON.stringify({ url: url, exclude: exclude }) );
        form.append(hiddenField);
    });

    var hiddenField = document.createElement("input");
    hiddenField.setAttribute("type", "hidden");
    hiddenField.setAttribute("name", "unitId");
    hiddenField.setAttribute("value", unitId );
    form.append(hiddenField);

    form.submit();
}

</script>
</head>
<body>

<div class="panel thumbs">
<?php
    echo sprintf(
        '<span class="title">%s (<span class="count-selected">%s</span>/%s)</span>',
        $batch["scientific_name"],count($batch["images"]),count($batch["images"]));
?>
    <input type="button" name="save" value="save" onclick="save();">
    <div class="thumbs-content">
    </div>
</div>

<div class="panel images">
</div>

<script type="text/javascript">
$(document).ready(function()
{
    $.fn.classList = function() {return this[0].className.split(/\s+/);};

    acquireInlineTemplates();

<?php
    echo "scientific_name = '".$batch["scientific_name"]."';\n";
    
    foreach ($batch["images"] as $key => $val)
    {
        echo "addImage('".$val['url']."','".$val['unitid']."');\n";
    }
?>
    printThumbnails();
    printImages();
    attachEvents();
});

</script>

<div class="inline-templates" id="imageTableTpl">
<!--
    <table>
        %body%
    </table>
-->
</div>

<div class="inline-templates" id="imageImageTpl">
<!--
    <tr data-key="%key%">
        <td class="medialib">%key%</td>
        <td class="medialib divider">
            <img src="%src%" data-key="%key%" class="medialib" title="%src%"/>
            <div class="exclude" data-key="%key%">%exclude%</div><br />
            %unitid%
        </td>
        <td class="divider">%up%</td>
        <td class="divider">%down%</td>
        <td class="divider"><img class="icon delete" data-key="%key%" src="img/x-png-icon-25.jpg"></td>
    </tr>
-->
</div>

<div class="inline-templates" id="imageIconUpTpl">
<!--
    <img class="icon up" data-key="%key%" src="img/svg-black-up-arrow-icon-1.png">
-->
</div>

<div class="inline-templates" id="imageIconDownTpl">
<!--
    <img class="icon down" data-key="%key%" src="img/svg-black-down-arrow-icon-1.png">
-->
</div>

<div class="inline-templates" id="thumbsTableTpl">
<!--
    <table><tr>
        %body%
    </tr></table>
-->
</div>

<div class="inline-templates" id="thumbsTdTpl">
<!--
    <td data-key="%key%" class="mediathumb">
        <img src="%src%" data-key="%key%" class="thumb"/>
        <div class="thumb exclude" data-key="%key%" >%exclude%</div><br />
        %unitid%
    </td>
-->
</div>

<div class="inline-templates" id="thumbsRowEndTpl">
<!--
    </tr><tr>
-->
</div>

<div class="inline-templates" id="overlayCrossTpl">
<!--
&#10799;
-->
</div>

</body>
