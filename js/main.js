var unitId="";
var images=Array();
var thumbsPerLine = 8;

function addImage( img )
{
    images.push( { url:img, key: images.length, exclude: false } );
}

function printThumbnails()
{
    buffer = Array();
    for(var i=0;i<images.length;i++)
    {
        buffer.push(fetchTemplate( 'thumbsTdTpl' )
            .replace('%src%',images[i].url.replace('large', 'medium'))
            .replace(/%key%/g,images[i].key)
            .replace('%exclude%',fetchTemplate( 'overlayCrossTpl' ))
        );

        if (i>0 && ((i+1) % thumbsPerLine)==0)
        {
            buffer.push(fetchTemplate( 'thumbsRowEndTpl' ));
        }
    }

    $('div.thumbs-content').html(fetchTemplate( 'thumbsTableTpl' ).replace('%body%',buffer.join("\n")));
}

function printImages()
{
    buffer = Array();
    for(var i=0;i<images.length;i++)
    {
        buffer.push(fetchTemplate( 'imageImageTpl' )
//            .replace('%up%',i>0 ? fetchTemplate( 'imageIconUpTpl' ):'')
//            .replace('%down%',i==(images.length-1) ? '' : fetchTemplate( 'imageIconDownTpl' ))
            .replace('%up%',fetchTemplate( 'imageIconUpTpl' ))
            .replace('%down%',fetchTemplate( 'imageIconDownTpl' ))
            .replace(/%src%/g,images[i].url)
            .replace(/%key%/g,images[i].key)
            .replace('%exclude%',fetchTemplate( 'overlayCrossTpl' ))
            
        );
    }

    $('div.images').html(fetchTemplate( 'imageTableTpl' ).replace('%body%',buffer.join("\n")));
}

function moveUpward(key)
{
    var row = $('tr[data-key='+key+']').closest('tr');
    var td = $('td.mediathumb[data-key='+key+']').closest('td');

    row.prev().insertAfter(row);
    td.prev().insertAfter(td);    
}

function attachEvents()
{
    $('img.icon.delete,img.thumb,.exclude').on('click',function()
    {
        var key = $(this).attr('data-key');
        for(var i=0;i<images.length;i++)
        {
            if (images[i].key==key)
            {
                images[i].exclude = !images[i].exclude;
                $('.exclude[data-key='+key+']').toggle(images[i].exclude);
            }
        }

        var excluded=0;
        for(var i=0;i<images.length;i++)
        {
            excluded += images[i].exclude ? 1 : 0;
        }
        $('.count-selected').html(images.length-excluded);
        highLightFirstImage();
    });

    $('img.icon.up,img.icon.down').on('click',function()
    {
        var downwards = $.inArray( "down", $(this).classList())==1;
        var key = $(this).attr('data-key');
        for(var i=0;i<images.length;i++)
        {
            if (images[i].key==key)
            {
                if (downwards)
                {
                    var row = $('tr[data-key='+key+']').closest('tr');
                    var td = $('td.mediathumb[data-key='+key+']').closest('td');
                    row.insertAfter( row.next() );
                    td.insertAfter( td.next() );                    
                }
                else
                {
                    moveUpward(key);
                }
            }
        }
        highLightFirstImage();
    });

}

function save()
{
    var form = $('<form action="" method="POST"></form>');
    form.appendTo('body')
    var n=0;
    $('img.medialib').each(function()
    {
        var key = $(this).attr('data-key');
        for(var i=0;i<images.length;i++)
        {
            if (images[i].key==key)
            {
                var exclude = images[i].exclude;
                var url = images[i].url;
            }
        }

        var hiddenField = document.createElement("input");
        hiddenField.setAttribute("type", "hidden");
        hiddenField.setAttribute("name", "images["+(n++)+"]");
        hiddenField.setAttribute("value", JSON.stringify({ url: url, exclude: exclude }) );
        form.append(hiddenField);
    });

    var hiddenField = document.createElement("input");
    hiddenField.setAttribute("type", "hidden");
    hiddenField.setAttribute("name", "unitId");
    hiddenField.setAttribute("value", unitId );
    form.append(hiddenField);

    form.submit();
}

function skip()
{
    var form = $('<form action="" method="POST"></form>');
    form.appendTo('body')

    var hiddenField = document.createElement("input");
    hiddenField.setAttribute("type", "hidden");
    hiddenField.setAttribute("name", "unitId");
    hiddenField.setAttribute("value", unitId );
    form.append(hiddenField);

    var hiddenField = document.createElement("input");
    hiddenField.setAttribute("type", "hidden");
    hiddenField.setAttribute("name", "action");
    hiddenField.setAttribute("value", "skip" );
    form.append(hiddenField);

    form.submit();
}

function popupimage(ele)
{
    window.open($(ele).attr('src').replace('/small','/large'),"_popup");
}

function toTheTop(ele)
{
    var key = $(ele).attr('data-key');

    for(var n=0;n<images.length;n++)
    {
        for(var i=0;i<images.length;i++)
        {
            if (images[i].key==key)
            {
                moveUpward(key);

            }
        }
    }
    highLightFirstImage();
}

function highLightFirstImage()
{
    // some other time 
}
